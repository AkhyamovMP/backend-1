<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../footer.css">
    <link rel="stylesheet" href="./style.css">
    <style>
        body {
            margin: 0;
        }

        .header {
            display: flex;
            justify-content: center;
            background-color: gray;
        }

        .logo-text {
            font-size: 20px;
            display: flex;
            align-items: center;
            margin-left: 20px;
        }
    </style>

</head>

<body>
    <header class="header">
        <img class="logo" src="./mospolytech-logo-white.png" alt="">
        <p class="logo-text">Самостоятельная работа "Feedback form"</p>
    </header>

    <main class="main">
        <section class="subscribe">
            <div class="subscribe__main-wrapper">
                <h1 class="subscribe__title">Feedback form</h1>

                <form class="subscribe__form" method="post" action="//httpbin.org/post">

                    <div class="subscribe__data-wrapper">
                        <fieldset class="subscribe__fieldset">
                            <legend class="visually-hidden">Input your personal information</legend>
                            <ul class="subscribe__list">
                                <li class="subscribe__item">
                                    <label for="name" class="subscribe__input-wrapper">
                                        <span class="subscribe__field-name">Имя</span>
                                        <input id="name" class="subscribe__input" type="text" name="full name" placeholder="Enter your full name" required>
                                    </label>
                                </li>
                                <li class="subscribe__item">
                                    <label for="email" class="subscribe__input-wrapper">
                                        <span class="subscribe__field-name">Email</span>
                                        <input id="email" class="subscribe__input" type="email" name="email" placeholder="Enter your email adress" required>
                                    </label>
                                </li>

                                <li class="subscribe__item">
                                    <div class="subscribe__input-wrapper">
                                        <span class="subscribe__field-name">Тип обращения</span>
                                        <ul class="card-slider__slider-list">
                                            <li class="card-slider__slider-item">
                                                <label for="type-of-complain__complain" class="subscribe__radio">
                                                    <span class="card-slider__complaine-type">Жалоба</span>
                                                    <input id="type-of-complain__complain" type="radio" name="type-of-complain" value="Жалоба" class="card-slider__button" required>
                                                </label>
                                            </li>
                                            <li class="card-slider__slider-item">
                                                <label for="type-of-complain__advice" class="subscribe__radio">
                                                    <span class="card-slider__complaine-type">Предложение</span>
                                                    <input id="type-of-complain__advice" type="radio" name="type-of-complain" value="Предложение" class="card-slider__button">
                                                </label>
                                            </li>
                                            <li class="card-slider__slider-item">
                                                <label for="type-of-complain__glad" class="subscribe__radio">
                                                    <span class="card-slider__complaine-type">Благодарность</span>
                                                    <input id="type-of-complain__glad" type="radio" name="type-of-complain" value="Благодарность" class="card-slider__button">
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="subscribe__item subscribe__item--extended">
                                    <label for="complain-text" class="subscribe__input-wrapper">
                                        <span class="subscribe__field-name">Текст обращения</span>
                                        <textarea name="complain-text" class="subscribe__input subscribe__input--extended" id="complain-text" cols="30" rows="10" required></textarea>
                                    </label>
                                </li>

                                <li class="subscribe__item">
                                    <span class="subscribe__field-name">В какой форме вам ответить?</span>
                                    <ul class="card-slider__slider-list-time">
                                        <li class="card-slider__slider-item">
                                            <label class="subscribe__radio">
                                                <input type="checkbox" name="feedback-form" value="sms" class="card-slider__time-to-call-button">
                                                <span class="card-slider__time-to-call">СМС</span>
                                            </label>
                                        </li>
                                        <li class="card-slider__slider-item">
                                            <label class="subscribe__radio">
                                                <input type="checkbox" name="feedback-form" value="email" class="card-slider__time-to-call-button">
                                                <span class="card-slider__time-to-call">Email</span>
                                            </label>
                                        </li>
                                    </ul>
                                </li>
                        </fieldset>
                    </div>
                    <div class="subscribe__button-wrapper">
                        <a href="./main2.php" class="subscribe__link">Ссылка на вторую страницу</a>
                        <button class="subscribe__submit" type="submit">Send
                            <svg class="subscribe__button-image" width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.91196 10H2.99996L1.02296 2.13505C1.0103 2.08934 1.00259 2.0424 0.999959 1.99505C0.977959 1.27405 1.77196 0.774048 2.45996 1.10405L21 10L2.45996 18.896C1.77996 19.223 0.995959 18.737 0.999959 18.029C1.00198 17.9658 1.0131 17.9031 1.03296 17.843L2.49996 13" stroke="#173753" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </button>
                    </div>
                </form>
            </div>
        </section>
    </main>

    <footer class="footer">
        <div class="footer__wrapper">
            <div class="footer__group-wrapper">
                <span class="footer__group">211-321</span>
                <span class="footer__name">Ахьямов Артур</span>
            </div>

            <div class="footer__date-wrapper">
                <span class="footer__date" id="dateFolder">03.03.2022</span>
            </div>
        </div>

    </footer>
</body>

</html>